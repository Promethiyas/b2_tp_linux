# Module 6 : Automatiser le déploiement

## 1-Enoncé

Bon bah là je vais pas beaucoup écrire pour ce sujet.

C'est simple : automatiser via un script `bash` le TP2. Tout ce que vous avez fait à la main dans le TP2 pour déployer un NextCloud et sa base de données, le but ici est de le faire de façon automatisée *via* un script `bash`.


➜ **Ecrire le script `bash`**

- il s'appellera `tp3_automation_nextcloud.sh`
- le script doit commencer par un *shebang* qui indique le chemin du programme qui exécutera le contenu du script
  - ça ressemble à ça si on veut utiliser `/bin/bash` pour exécuter le contenu de notre script :

```
#!/bin/bash
```

- le script :
  - à partir d'une machine vierge (enfin, notre patron de cours Rocky quoi)
  - il installe complètement Apache et NextCloud
  - on lance le script, et on va prendre un café
- NB : le script ne comportera aucun `sudo` car il sera directement lancé avec les droits de `root`, question de bonnes pratiques :

```bash
$ sudo ./tp3_automation_nextcloud.sh
```

> Il peut être intéressant de tester la fonctionnalité de *snapshot* de VirtualBox afin de sauvegarder une VM de test dans un état vierge, et pouvoir la restaurer facilement dans cet état vierge plus tard.

✨ **Bonus** : faire un deuxième script `tp3_automation_db.sh` qui automatise l'installation de la base de données de NextCloud

## 2-Installation

- On installe le fichier [tp3_automation_nextcloud.sh](./tp3/6-automatisation/tp3_automation_nextcloud.sh) sur notre ordinateur

- On fait passer le fichier sur notre machine vierge (ici `testscript`)

```bash
 scp C:\Users\bravo\Downloads\tp3_automation_nextcloud.sh root@10.102.1.18:/home/val/test
 ```

- On lance l'installe et on va prendre notre café

```bash
[val@localhost ~]$ sudo bash test/tp3_automation_nextcloud.sh
```

- Et c'est bon tout est prêt
