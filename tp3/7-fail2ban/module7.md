# Module 7 : Fail2Ban

## 1-Enoncé
Fail2Ban c'est un peu le cas d'école de l'admin Linux, je vous laisse Google pour le mettre en place.

C'est must-have sur n'importe quel serveur à peu de choses près. En plus d'enrayer les attaques par bruteforce, il limite aussi l'imact sur les performances de ces attaques, en bloquant complètement le trafic venant des IP considérées comme malveillantes

Faites en sorte que :

- si quelqu'un se plante 3 fois de password pour une co SSH en moins de 1 minute, il est ban
- vérifiez que ça fonctionne en vous faisant ban
- afficher la ligne dans le firewall qui met en place le ban
- lever le ban avec une commande liée à fail2ban

> Vous pouvez vous faire ban en effectuant une connexion SSH depuis `web.tp2.linux` vers `db.tp2.linux` par exemple, comme ça vous gardez intacte la connexion de votre PC vers `db.tp2.linux`, et vous pouvez continuer à bosser en SSH.

## 2-Mise en place

- **On commence par installer fail2ban**

```bash
[val@proxy ~]$ sudo dnf install fail2ban fail2ban-firewalld
Extra Packages for Enterprise Linux 9 - x86_64                                                                              3.4 MB/s |  12 MB     00:03
Last metadata expiration check: 0:00:11 ago on Tue 22 Nov 2022 03:24:19 PM CET.
Dependencies resolved.
[...]
```

- **On demarre, met en démarrage automatique le service**

```bash
[val@proxy ~]$ sudo systemctl start fail2ban
[val@proxy ~]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
[val@proxy ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
     Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-11-22 15:25:16 CET; 11s ago
       Docs: man:fail2ban(1)
   Main PID: 1603 (fail2ban-server)
      Tasks: 3 (limit: 48946)
     Memory: 12.3M
        CPU: 256ms
     CGroup: /system.slice/fail2ban.service
             └─1603 /usr/bin/python3 -s /usr/bin/fail2ban-server -xf start

Nov 22 15:25:16 proxy.tp3.linux systemd[1]: Starting Fail2Ban Service...
Nov 22 15:25:16 proxy.tp3.linux systemd[1]: Started Fail2Ban Service.
Nov 22 15:25:16 proxy.tp3.linux fail2ban-server[1603]: 2022-11-22 15:25:16,804 fail2ban.configreader   [1603]: WARNING 'allowipv6' not defined in 'Definiti>
Nov 22 15:25:16 proxy.tp3.linux fail2ban-server[1603]: Server ready
```

- **Pour ne pas modifier directement `jail.conf` on le copie**

```bash
[val@proxy ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
```

- **On fait en sorte que fail2ban fonctionne avec `firewalld` et non `iptable`**

```bash
[val@proxy ~]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local
```

- **Et on restart**

```bash
[val@proxy ~]$ sudo systemctl restart fail2ban
```

- **On va créer une "prison"**

```bash
[val@proxy ~]$ sudo vim /etc/fail2ban/jail.d/sshd.local
[val@proxy ~]$ sudo cat /etc/fail2ban/jail.d/sshd.local
[sshd]
enabled = true

# Override the default global configuration
# for specific jail sshd
bantime = 10m
maxretry = 3
```

- **On restart**

```bash
[val@proxy ~]$ sudo systemctl restart fail2ban
```

- **On voit que maintenant nous avons notre prison**

```bash
[val@proxy ~]$ sudo fail2ban-client status
Status
|- Number of jail:      1
`- Jail list:   sshd
```

## 3-Test

- **Quand on essaye 3 fois de se connecter nous nous mangeons une connection refused**

```bash
[val@web ~]$ ssh val@10.102.1.13
The authenticity of host '10.102.1.13 (10.102.1.13)' can't be established.
ED25519 key fingerprint is SHA256:nmKLJXCQWhry97nJGwX/9osqdQZE8sRmsUrKaW4DbeU.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Failed to add the host to the list of known hosts (/home/val/.ssh/known_hosts).
val@10.102.1.13's password:
Permission denied, please try again.
val@10.102.1.13's password:
Permission denied, please try again.
val@10.102.1.13's password:
val@10.102.1.13: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
[val@web ~]$ ssh val@10.102.1.13
ssh: connect to host 10.102.1.13 port 22: Connection refused
```

- **Et depuis notre vm nous pouvons voir les informations de la personne en prison**

```bash
[val@proxy ~]$ sudo fail2ban-client status
Status
|- Number of jail:      1
`- Jail list:   sshd
[val@proxy ~]$ sudo fail2ban-client get sshd maxretry
3
[val@proxy ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     3
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 1
   |- Total banned:     1
   `- Banned IP list:   10.102.1.11
```
- **On peut voir la ligne du truc du `firewalld`**

```bash
[val@proxy ~]$ sudo firewall-cmd --list-rich-rules
rule family="ipv4" source address="10.102.1.11" port port="ssh" protocol="tcp" reject type="icmp-port-unreachable"
```

- **On peut unban la personne**

```bash
[val@proxy ~]$ sudo fail2ban-client unban 10.102.1.11
1
```

- **Et essayé de se reconnecter**

```bash
[val@web ~]$ ssh val@10.102.1.13
The authenticity of host '10.102.1.13 (10.102.1.13)' can't be established.
ED25519 key fingerprint is SHA256:nmKLJXCQWhry97nJGwX/9osqdQZE8sRmsUrKaW4DbeU.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Failed to add the host to the list of known hosts (/home/val/.ssh/known_hosts).
val@10.102.1.13's password:
Last failed login: Tue Nov 22 15:35:00 CET 2022 from 10.102.1.11 on ssh:notty
There were 3 failed login attempts since the last successful login.
Last login: Tue Nov 22 14:52:20 2022 from 10.102.1.1
```

- **Et voir qu'il n'est plus dans la liste des bannis**

```bash
[val@proxy ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     3
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 0
   |- Total banned:     1
   `- Banned IP list:
```
