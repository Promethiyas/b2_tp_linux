# Module 5 : Monitoring

Dans ce sujet on va installer un outil plutôt clé en main pour mettre en place un monitoring simple de nos machines.


Installez-le sur `web.tp2.linux` et `db.tp2.linux`.

**Sur `web.tp2.linux` et  `db.tp2.linux`**

```bash
[val@db ~]$ sudo dnf install epel-release -y
```

- *puis*

```bash
[val@web ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
```

- *et*

```bash
[val@web ~]$ sudo systemctl start netdata
[val@web ~]$ sudo systemctl enable netdata
[val@web ~]$ sudo systemctl status netdata
```

- *on continue*

```bash
[val@web ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success
[val@web ~]$ sudo  firewall-cmd --reload
success
```

- *et voici pour cette partie*

![](https://cdn.discordapp.com/attachments/905799668938723329/1043128780631330906/image.png)


➜ **Configurer Netdata pour qu'il vous envoie des alertes** dans [un salon Discord](https://learn.netdata.cloud/docs/agent/health/notifications/discord) dédié en cas de soucis

-on doit créer un Webhook

![](https://cdn.discordapp.com/attachments/905799668938723329/1043132537263894620/image.png)

- *on modifie `/etc/netdata/edit-config health_alarm_notify.conf`*
```bash
[val@db ~]$ sudo cat /etc/netdata/health_alarm_notify.conf
SEND_DISCORD="YES"

DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1043130101195362334/fBxsqFfgncKphrF8f5HfUqNqqSGVKbHE1cQUF_tIfjzwSoAbFo53kcOOP9QENNx8RlfK"

DEFAULT_RECIPIENT_DISCORD="alarms"
DEFAULT_RECIPIENT_SLACK="#"
```
- *on remplace le lien du Webhook par le notre et techniquement ça devrait être bon*

- *et voilaaaa*
![](https://cdn.discordapp.com/attachments/1043129905178746894/1044218341352558592/image.png)
