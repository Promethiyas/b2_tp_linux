# Module 1 : Reverse Proxy

## Sommaire

- [Module 1 : Reverse Proxy](#module-1--reverse-proxy)
  - [Sommaire](#sommaire)
- [I. Intro](#i-intro)
- [II. Setup](#ii-setup)
- [III. HTTPS](#iii-https)

# I. Intro

# II. Setup

🖥️ **VM `proxy.tp3.linux`**

**N'oubliez pas de dérouler la [📝**checklist**📝](#checklist).**

➜ **On utilisera NGINX comme reverse proxy**

- installer le paquet `nginx`

```bash
[val@proxy ~]$ sudo dnf install nginx
[sudo] password for val:
Rocky Linux 9 - BaseOS                                                                  6.6 kB/s | 3.6 kB     00:00
Rocky Linux 9 - BaseOS                                                                  1.8 MB/s | 1.7 MB     00:00
[...]
Installed:
  nginx-1:1.20.1-10.el9.x86_64     nginx-filesystem-1:1.20.1-10.el9.noarch     rocky-logos-httpd-90.11-1.el9.noarch

Complete!
```

- démarrer le service `nginx`

```bash
[val@proxy ~]$ sudo systemctl start nginx
[val@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-17 11:03:53 CET; 9s ago
    Process: 1399 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 1400 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 1401 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
[...]
Nov 17 11:03:53 proxy.tp3.linux systemd[1]: Started The nginx HTTP and reverse proxy server.
```

- utiliser la commande `ss` pour repérer le port sur lequel NGINX écoute

```bash
[val@proxy ~]$ sudo ss -alnpt | grep -i nginx
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=1403,fd=6),("nginx",pid=1402,fd=6))
LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=1403,fd=7),("nginx",pid=1402,fd=7))
```

- ouvrir un port dans le firewall pour autoriser le trafic vers NGINX

```bash
[val@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[val@proxy ~]$ sudo firewall-cmd --reload
success
```

- utiliser une commande `ps -ef` pour déterminer sous quel utilisateur tourne NGINX

```bash
[val@proxy ~]$ ps -ef  | grep -i nginx
root        1402       1  0 11:03 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1403    1402  0 11:03 ?        00:00:00 nginx: worker process
val         1439    1225  0 11:09 pts/0    00:00:00 grep --color=auto -i nginx
```

- vérifier que le page d'accueil NGINX est disponible en faisant une requête HTTP sur le port 80 de la machine

```bash
[val@web ~]$ curl 10.102.1.13:80
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
[...]
```

➜ **Configurer NGINX**

- nous ce qu'on veut, c'pas une page d'accueil moche, c'est que NGINX agisse comme un reverse proxy entre les clients et notre serveur Web
- deux choses à faire :
  - créer un fichier de configuration NGINX
    - la conf est dans `/etc/nginx`
    - procédez comme pour Apache : repérez les fichiers inclus par le fichier de conf principal, et créez votre fichier de conf en conséquence

```bash
[val@proxy ~]$ sudo cat /etc/nginx/conf.d/clair.conf
server {
    server_name web.tp2.linux;

    listen 80;
[...]
}
```

```bash
[val@proxy ~]$ sudo systemctl restart nginx
[val@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-17 12:06:41 CET; 5s ago
    Process: 1552 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 1553 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
```

  - NextCloud est un peu exigeant, et il demande à être informé si on le met derrière un reverse proxy
    - y'a donc un fichier de conf NextCloud à modifier
    - c'est un fichier appelé `config.php`

```bash
[val@web ~]$ sudo cat /var/www/tp2_nextcloud/config/config.php
<?php
$CONFIG = array (
  'instanceid' => 'ocqcj9l1rlu0',
  'passwordsalt' => 'oN5mQo+4fy74sxff2JanTbdd+NTSXt',
  'secret' => 'e2Vr9x8weVUg8hvy9S/VprmFNcMQWtRPIhKospKB8GuhEbTN',
  'trusted_domains' => 'proxy.tp3.linux'
  'trusted_proxies' => '10.102.1.13'
[...]
);
```


➜ **Modifier votre fichier `hosts` de VOTRE PC**

- pour que le service soit joignable avec le nom `web.tp2.linux`
- c'est à dire que `web.tp2.linux` doit pointer vers l'IP de `proxy.tp3.linux`
- autrement dit, pour votre PC :
  - `web.tp2.linux` pointe vers l'IP du reverse proxy

```bash
PS C:\Users\bravo> ping web.tp2.linux

Envoi d’une requête 'ping' sur web.tp2.linux [10.102.1.13] avec 32 octets de données :
Réponse de 10.102.1.13 : octets=32 temps<1ms TTL=64
```

  - `proxy.tp3.linux` ne pointe vers rien

```bash
PS C:\Users\bravo> ping proxy.tp2.linux
La requête Ping n’a pas pu trouver l’hôte proxy.tp2.linux. Vérifiez le nom et essayez à nouveau.
```

  - taper `http://web.tp2.linux` permet d'accéder au site (en passant de façon transparente par l'IP du proxy)

```bash
PS C:\Users\bravo> curl http://web.tp2.linux


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html class="ng-csp" data-placeholder-focus="false" lang="en" data-locale="en" >
                        <head
                     data-requesttoken="yzYdBIe7R1TxKEtLDlv6OzIxKo+XDpQm6HLvgZ8tbxc=:ilxMVOrTCD7DTCQCOx+bUXVkfv2mQPV...
RawContent        : HTTP/1.1 200 OK
```

> Oui vous ne rêvez pas : le nom d'une machine donnée pointe vers l'IP d'une autre ! Ici, on fait juste en sorte qu'un certain nom permette d'accéder au service, sans se soucier de qui porte réellement ce nom.

✨ **Bonus** : rendre le serveur `web.tp2.linux` injoignable sauf depuis l'IP du reverse proxy. En effet, les clients ne doivent pas joindre en direct le serveur web : notre reverse proxy est là pour servir de serveur frontal.

- faire un `ping` manuel vers l'IP de `proxy.tp3.linux` fonctionne

```bash
PS C:\Users\bravo> ping 10.102.1.13

Envoi d’une requête 'Ping'  10.102.1.13 avec 32 octets de données :
Réponse de 10.102.1.13 : octets=32 temps<1ms TTL=64
Réponse de 10.102.1.13 : octets=32 temps<1ms TTL=64
```

- faire un `ping` manuel vers l'IP de `web.tp2.linux` ne fonctionne pas

# III. HTTPS

Le but de cette section est de permettre une connexion chiffrée lorsqu'un client se connecte. Avoir le ptit HTTPS :)

Le principe :

- on génère une paire de clés sur le serveur `proxy.tp3.linux`
  - une des deux clés sera la clé privée : elle restera sur le serveur et ne bougera jamais
  - l'autre est la clé publique : elle sera stockée dans un fichier appelé *certificat*
    - le *certificat* est donné à chaque client qui se connecte au site

```bash
[val@proxy ~]$ ls -l
total 8
-rw-r--r--. 1 val val 1444 Nov 21 10:57 certficat.crt
-rw-------. 1 val val 1704 Nov 21 10:56 server.key
```

- on ajuste la conf NGINX
  - on lui indique le chemin vers le certificat et la clé privée afin qu'il puisse les utiliser pour chiffrer le trafic
  - on lui demande d'écouter sur le port convetionnel pour HTTPS : 443 en TCP

```bash
[val@proxy ~]$ sudo cat /etc/nginx/conf.d/clair.conf
server {
    server_name web.tp2.linux;

    listen 443 ssl;

    ssl                       on;
    ssl_certificate           /home/val/certficat.crt;
    ssl_certificate_key       /home/val/server.key;
    ssl_protocols             TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;

    location / {
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        proxy_pass https://10.102.1.11:80;
    }

    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```

- *on ouvre le port `443`*

```bash
[val@proxy ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[val@proxy ~]$ sudo firewall-cmd --reload
success
```
**TADAAAAAA**

![](https://cdn.discordapp.com/attachments/1043129905178746894/1044210071502073906/Capture_decran_2022-11-21_a_12.17.27.png)
