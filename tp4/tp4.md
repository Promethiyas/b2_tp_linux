# TP4 : Conteneurs

# Sommaire

- [TP4 : Conteneurs](#tp4--conteneurs)
- [Sommaire](#sommaire)
- [I. Docker](#i-docker)
  - [1. Install](#1-install)
  - [2. Vérifier l'install](#2-vérifier-linstall)
  - [3. Lancement de conteneurs](#3-lancement-de-conteneurs)
- [II. Images](#ii-images)
- [III. `docker-compose`](#iii-docker-compose)
  - [1. Make your own meow](#1-make-your-own-meow)

# I. Docker

## 1. Install

- **Installer Docker sur la machine**
  - **On installe le repo**

```bash
[val@docker1 ~]$ sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

  - **On installe Docker**

```bash
[val@docker1 ~]$ sudo dnf install docker-ce docker-ce-cli containerd.io
```

- **démarrer le service `docker` avec une commande `systemctl`**

```bash
[val@docker1 ~]$ sudo systemctl start docker
[val@docker1 ~]$ sudo systemctl status docker
● docker.service - Docker Application Container Engine
     Loaded: loaded (/usr/lib/systemd/system/docker.service; disabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-24 10:56:17 CET; 4s ago
[...]
```

- **ajouter votre utilisateur au groupe `docker`**
  - avec la commande : `sudo usermod -aG docker $(whoami)`

```bash
[val@docker1 ~]$ sudo usermod -aG docker $(whoami)
```


## 2. Vérifier l'install

➜ **Vérifiez que Docker est actif est disponible en essayant quelques commandes usuelles :**

- **Info sur l'install actuelle de Docker**
```bash
[val@docker1 ~]$ docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
[...]
```

- **Liste des conteneurs actifs**
```bash
[val@docker1 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

- **Liste de tous les conteneurs**
```bash
[val@docker1 ~]$ docker ps -a
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

- **Liste des images disponibles localement**
```bash
[val@docker1 ~]$ docker images
REPOSITORY   TAG       IMAGE ID   CREATED   SIZE
```

- **Lancer un conteneur debian**

```bash
[val@docker1 ~]$ docker run debian
Unable to find image 'debian:latest' locally
latest: Pulling from library/debian
a8ca11554fce: Pull complete
Digest: sha256:3066ef83131c678999ce82e8473e8d017345a30f5573ad3e44f62e5c9c46442b
Status: Downloaded newer image for debian:latest
```

```bash
[val@docker1 ~]$ docker run -d debian sleep 99999
ed5a9ecf479b31c1282c56d85c5db94d4b129fdf378b3c26a292240ef1a69bb4
```

```bash
[val@docker1 ~]$ docker run -it debian bash
root@cb7b5766f772:/#
```

- **Consulter les logs d'un conteneur**
```bash
[val@docker1 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND         CREATED              STATUS              PORTS     NAMES
ed5a9ecf479b   debian    "sleep 99999"   About a minute ago   Up About a minute             naughty_pascal
```

```bash
[val@docker1 ~]$ docker logs 28b54bf4675f
root@28b54bf4675f:/# etrytt
bash: etrytt: command not found
root@28b54bf4675f:/# rytyku
bash: rytyku: command not found
root@28b54bf4675f:/#
root@28b54bf4675f:/# exit
exit
```

- **Exécuter un processus dans un conteneur actif**
```bash
[val@docker1 ~]$ docker exec ed5a9ecf479b ls
bin
boot
dev
etc
home
lib
lib64
media
mnt
opt
proc
root
run
sbin
srv
sys
tmp
usr
var
```

## 3. Lancement de conteneurs


- **lancer un conteneur `nginx`**

```bash
[val@docker1 ~]$ docker run -it nginx bash
Unable to find image 'nginx:latest' locally
latest: Pulling from library/nginx
[...]
```

  - **l'app NGINX doit avoir un fichier de conf personnalisé**

```bash
[val@docker1 ~]$ docker run --name web -d -v /etc/nginx/nginx.conf:/etc/nginx/ -p 8080:80 nginx
f4e0be61c151c2b27623cebfeda6b5f3374f4f906a42527a012a2a4772bd869f
```

  - **l'app NGINX doit servir un fichier `index.html` personnalisé**

```bash
PS C:\Users\bravo> curl 10.104.1.11:8080


StatusCode        : 200
StatusDescription : OK
Content           : <html>
                    <header><title>tudum</title></header>
                    <body>
                    I'm a nation,
                    I'm a million faces
                    </body>
                    </html>
```

  - **l'application doit être joignable grâce à un partage de ports**

**ker run --name web -v /home/val/index.html:/usr/share/nginx/html/index.html -v /etc/nginx/nginx.conf:/etc/nginx/nginx.conf --rm -p `8080:80` nginx**

  - **vous limiterez l'utilisation de la RAM et du CPU de ce conteneur**
    - **Limité à `6m` pour la mémoire (le minimum) et `4m` pour le CPU (le minimum)**

**docker run --name web -v /home/val/index.html:/usr/share/nginx/html/index.html -v /etc/nginx/nginx.conf:/etc/nginx/nginx.conf `--memory=6m` `--kernel-memory=4m` --rm -p 8080:80 nginx**

  - **le conteneur devra avoir un nom**

**docker run --name `web` -v /home/val/index.html:/usr/share/nginx/html/index.html -v /etc/nginx/nginx.conf:/etc/nginx/nginx.conf --rm -p 8080:80 nginx**

# II. Images


## 1. Construisez votre propre Dockerfile

🌞 **Construire votre propre image**

- **On curl à `localhost:8888`

```bash
[val@docker1 Ubuntu]$ curl localhost:8888
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <!--
    Modified from the Debian original for Ubuntu
    Last updated: 2022-03-22
    See: https://launchpad.net/bugs/1966004
[...]
```


📁 [Dockerfile](./tp4/Dockerfile)



# III. `docker-compose`


## 1. Make your own meow

🌞 **Conteneurisez votre application**

  - le `cd` dans le bon dossier

```bash
[val@docker1 app]$ cd app
```
  - la commande `docker build` pour build l'image

```bash
[val@docker1 app]$ docker build . -t test
```

  - la commande `docker-compose` pour lancer le(s) conteneur(s)

```bash
[val@docker1 app]$ docker compose up
```

**DISCLAIMER: ça ne marche pas quand on lance `docker compose up` il y a une erreur par rapport au PATH de go, que je n'ai pas réussi à résoudre**

📁 [Dockerfile](./tp4/app/Dockerfile)
📁 [docker-compose](./tp4/app/docker-compose.yml)
