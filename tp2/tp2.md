# TP2 : Gestion de service

# Sommaire

- [TP2 : Gestion de service](#tp2--gestion-de-service)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro blabla](#1-intro-blabla)
  - [2. Setup](#2-setup)
    - [A. Base de données](#a-base-de-données)
    - [B. Serveur Web et NextCloud](#b-serveur-web-et-nextcloud)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# 0. Prérequis

➜ Machines Rocky Linux

➜ Un unique host-only côté VBox, ça suffira. **L'adresse du réseau host-only sera `10.102.1.0/24`.**

➜ Chaque **création de machines** sera indiquée par **l'emoji 🖥️ suivi du nom de la machine**

➜ Si je veux **un fichier dans le rendu**, il y aura l'**emoji 📁 avec le nom du fichier voulu**. Le fichier devra être livré tel quel dans le dépôt git, ou dans le corps du rendu Markdown si c'est lisible et correctement formaté.

## Checklist

A chaque machine déployée, vous **DEVREZ** vérifier la 📝**checklist**📝 :

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] SSH fonctionnel avec un échange de clé
- [x] accès Internet (une route par défaut, une carte NAT c'est très bien)
- [x] résolution de nom
- [x] SELinux désactivé (vérifiez avec `sestatus`, voir [mémo install VM tout en bas](https://gitlab.com/it4lik/b2-reseau-2022/-/blob/main/cours/memo/install_vm.md#4-pr%C3%A9parer-la-vm-au-clonage))

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

# I. Un premier serveur web

## 1. Installation

🖥️ **VM web.tp2.linux**

| Machine         | IP            | Service     |
|-----------------|---------------|-------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web |

🌞 **Installer le serveur Apache**

- paquet `httpd`
- la conf se trouve dans `/etc/httpd/`
  - le fichier de conf principal est `/etc/httpd/conf/httpd.conf`
  - je vous conseille **vivement** de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair
    - avec `vim` vous pouvez tout virer avec `:g/^ *#.*/d`

```bash
[val@web ~]$ sudo cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache

[...]
```

🌞 **Démarrer le service Apache**

- le service s'appelle `httpd` (raccourci pour `httpd.service` en réalité)
  - démarrez le

```bash
[val@web ~]$ sudo systemctl start httpd
```

  - faites en sorte qu'Apache démarre automatique au démarrage de la machine

```bash
[val@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```

  - ouvrez le port firewall nécessaire
    - utiliser une commande `ss` pour savoir sur quel port tourne actuellement Apache
```bash
[val@web ~]$ sudo ss -alnpt | grep -i httpd
LISTEN 0      511                *:80              *:*    users:(("httpd",pid=2081,fd=4),("httpd",pid=2080,fd=4),("httpd",pid=2079,fd=4),("httpd",pid=2077,fd=4))
```

```bash
[val@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[val@web ~]$ sudo firewall-cmd --reload
success
```
🌞 **TEST**

- vérifier que le service est démarré

```bash
[val@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.servic>
     Active: active (running) since Tue 2022-11-15 10:20:>
       Docs: man:httpd.service(8)
   Main PID: 2077 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;>
      Tasks: 213 (limit: 48946)
     Memory: 25.2M
        CPU: 492ms
     CGroup: /system.slice/httpd.service
             ├─2077 /usr/sbin/httpd -DFOREGROUND
             ├─2078 /usr/sbin/httpd -DFOREGROUND
             ├─2079 /usr/sbin/httpd -DFOREGROUND
             ├─2080 /usr/sbin/httpd -DFOREGROUND
             └─2081 /usr/sbin/httpd -DFOREGROUND

Nov 15 10:20:51 web.tp2.linux systemd[1]: Starting The Ap>
Nov 15 10:20:52 web.tp2.linux systemd[1]: Started The Apa>
Nov 15 10:20:52 web.tp2.linux httpd[2077]: Server configu>
lines 1-19/19 (END)
```

- vérifier qu'il est configuré pour démarrer automatiquement

```bash
[val@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```

- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement

```bash
[val@web ~]$ curl 10.102.1.11:80
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
[...]
```

```bash
bravo@Asus-Valentin MINGW64 ~
$ curl 10.102.1.11:80
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
[...]
```

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache

```bash
[val@web ~]$ sudo cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf principal d'Apache (`httpd.conf`) qui définit quel user est utilisé

```bash
[val@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep -i user
User apache
```

- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf

```bash
[val@web ~]$ ps -ef | grep -i Apache
apache      2078    2077  0 10:20 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2079    2077  0 10:20 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2080    2077  0 10:20 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2081    2077  0 10:20 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

- la page d'accueil d'Apache se trouve dans `/usr/share/testpage/`
  - vérifiez avec un `ls -al` que tout son contenu est **accessible en lecture** à l'utilisateur mentionné dans le fichier de conf

```bash
[val@web ~]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Nov 15 10:13 .
drwxr-xr-x. 82 root root 4096 Nov 15 10:13 ..
-rw-r--r--.  1 root root 7620 Jul  6 04:37 index.html
```

🌞 **Changer l'utilisateur utilisé par Apache**

- créez un nouvel utilisateur
  - pour les options de création, inspirez-vous de l'utilisateur Apache existant

```bash
[val@web ~]$ sudo useradd bob -m -s /sbin/nologin -u 2001
```

    - le fichier `/etc/passwd` contient les informations relatives aux utilisateurs existants sur la machine
    - servez-vous en pour voir la config actuelle de l'utilisateur Apache par défaut
  
```bash
[val@web ~]$ sudo cat /etc/passwd | grep -i apache
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
```
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur

```bash
[val@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep -i User
User bob
```

- redémarrez Apache

```bash
[val@web ~]$ sudo systemctl restart httpd
```

- utilisez une commande `ps` pour vérifier que le changement a pris effet

```bash
[val@web ~]$ ps aux | grep -i bob
bob         2601  0.0  0.0  21576  7408 ?        S    12:01   0:00 /usr/sbin/httpd -DFOREGROUND
bob         2602  0.0  0.1 1079376 10976 ?       Sl   12:01   0:00 /usr/sbin/httpd -DFOREGROUND
bob         2603  0.0  0.1 1079376 10976 ?       Sl   12:01   0:00 /usr/sbin/httpd -DFOREGROUND
bob         2604  0.0  0.1 1210512 13024 ?       Sl   12:01   0:00 /usr/sbin/httpd -DFOREGROUND
val         2827  0.0  0.0   6404  2212 pts/0    S+   12:03   0:00 grep --color=auto -i bob
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demander d'écouter sur un autre port de votre choix

```bash
[val@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep -i Listen
Listen 55
```

- ouvrez ce nouveau port dans le firewall, et fermez l'ancien

```bash
[val@web ~]$ sudo firewall-cmd --add-port=55/tcp --permanent
success
[val@web ~]$ sudo firewall-cmd --zone=public --remove-port=80/tcp--permanent
success
```

- redémarrez Apache

```bash
[val@web ~]$ sudo systemctl restart httpd
```

- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi

```bash
[val@web ~]$ sudo ss -alnpt | grep -i 55
LISTEN 0      511                *:55              *:*    users:(("httpd",pid=1489,fd=4),("httpd",pid=1488,fd=4),("httpd",pid=1487,fd=4),("httpd",pid=1485,fd=4))
```

- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port

```bash
[val@web ~]$ curl 10.102.1.11:55
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
```

- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port

```bash
bravo@Asus-Valentin MINGW64 ~
$ curl 10.102.1.11:55
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
```

📁 **Fichier `/etc/httpd/conf/httpd.conf`**

```bash
PS C:\Users\bravo\OneDrive\Bureau> scp val@10.102.1.11:/etc/httpd/conf/httpd.conf ./
httpd.conf                                100% 1509   456.4KB/s   00:00
```


# II. Une stack web plus avancée

⚠⚠⚠ **Réinitialiser votre conf Apache avant de continuer** ⚠⚠⚠  
En particulier :

- reprendre le port par défaut
- reprendre l'utilisateur par défaut

## 1. Intro blabla

**Le serveur web `web.tp2.linux` sera le serveur qui accueillera les clients.** C'est sur son IP que les clients devront aller pour visiter le site web.  

**Le service de base de données `db.tp2.linux` sera uniquement accessible depuis `web.tp2.linux`.** Les clients ne pourront pas y accéder. Le serveur de base de données stocke les infos nécessaires au serveur web, pour le bon fonctionnement du site web.

---

Bon le but de ce TP est juste de s'exercer à faire tourner des services, un serveur + sa base de données, c'est un peu le cas d'école. J'ai pas envie d'aller deep dans la conf de l'un ou de l'autre avec vous pour le moment, on va se contenter d'une conf minimale.

Je vais pas vous demander de coder une application, et cette fois on se contentera pas d'un simple `index.html` tout moche et on va se mettre dans la peau de l'admin qui se retrouve avec une application à faire tourner. **On va faire tourner un [NextCloud](https://nextcloud.com/).**

En plus c'est utile comme truc : c'est un p'tit serveur pour héberger ses fichiers via une WebUI, style Google Drive. Mais on l'héberge nous-mêmes :)

---

Le flow va être le suivant :

➜ **on prépare d'abord la base de données**, avant de setup NextCloud

- comme ça il aura plus qu'à s'y connecter
- ce sera sur une nouvelle machine `db.tp2.linux`
- il faudra installer le service de base de données, puis lancer le service
- on pourra alors créer, au sein du service de base de données, le nécessaire pour NextCloud

➜ **ensuite on met en place NextCloud**

- on réutilise la machine précédente avec Apache déjà installé, ce sera toujours Apache qui accueillera les requêtes des clients
- mais plutôt que de retourner une bête page HTML, NextCloud traitera la requête
- NextCloud, c'est codé en PHP, il faudra donc **installer une version de PHP précise** sur la machine
- on va donc : install PHP, configurer Apache, récupérer un `.zip` de NextCloud, et l'extraire au bon endroit !

![NextCloud install](./pics/nc_install.png)

## 2. Setup

🖥️ **VM db.tp2.linux**

**N'oubliez pas de dérouler la [📝**checklist**📝](#checklist).**

| Machines        | IP            | Service                 |
|-----------------|---------------|-------------------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données |

### A. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**
- je veux dans le rendu **toutes** les commandes réalisées

```bash
[val@db ~]$ sudo dnf install mariadb-server
[sudo] password for val:
Rocky Linux 9 - BaseOS                                                                                                      7.3 kB/s | 3.6 kB     00:00
Rocky Linux 9 - BaseOS                                                                                                      2.6 MB/s | 1.7 MB     00:00
Rocky Linux 9 - AppStream                                                                                                   9.6 kB/s | 3.6 kB     00:00
Rocky Linux 9 - AppStream                                                                                                   3.9 MB/s | 6.0 MB     00:01
Rocky Linux 9 - Extras                                                                                                      6.8 kB/s | 2.9 kB     00:00
[...]
```

```bash
[val@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```

```bash
[val@db ~]$ sudo systemctl start mariadb
[val@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.5 database server
     Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-11-15 15:21:03 CET; 7s ago
       Docs: man:mariadbd(8)
             https://mariadb.com/kb/en/library/systemd/
```

```bash
[val@db ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
```

- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp2.linux`

```bash
[val@db ~]$ sudo ss -alnpt | grep -i maria
LISTEN 0      80                 *:3306            *:*    users:(("mariadbd",pid=3504,fd=15))
```

  - il sera nécessaire de l'ouvrir dans le firewall

```bash
[val@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
```


> La doc vous fait exécuter la commande `mysql_secure_installation` c'est un bon réflexe pour renforcer la base qui a une configuration un peu *chillax* à l'install.

🌞 **Préparation de la base pour NextCloud**

- une fois en place, il va falloir préparer une base de données pour NextCloud :
  - connectez-vous à la base de données à l'aide de la commande `sudo mysql -u root -p`
  - exécutez les commandes SQL suivantes :

```sql
# Création d'un utilisateur dans la base, avec un mot de passe
# L'adresse IP correspond à l'adresse IP depuis laquelle viendra les connexions. Cela permet de restreindre les IPs autorisées à se connecter.
# Dans notre cas, c'est l'IP de web.tp2.linux
# "pewpewpew" c'est le mot de passe hehe
[...]
# C'est assez générique comme opération, on crée une base, on crée un user, on donne les droits au user sur la base
```


```bash
[val@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 11
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> quit
Bye
```

> Par défaut, vous avez le droit de vous connecter localement à la base si vous êtes `root`. C'est pour ça que `sudo mysql -u root` fonctionne, sans nous demander de mot de passe. Evidemment, n'importe quelles autres conditions ne permettent pas une connexion aussi facile à la base.

🌞 **Exploration de la base de données**

- afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :
  - depuis la machine `web.tp2.linux` vers l'IP de `db.tp2.linux`
  - utilisez la commande `mysql` pour vous connecter à une base de données depuis la ligne de commande
    - par exemple `mysql -u <USER> -h <IP_DATABASE> -p`

```bash
[val@web ~]$ sudo mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
[...]
mysql>
```

- si vous ne l'avez pas, installez-là

```bash
[val@web ~]$ sudo dnf install mysql
[sudo] password for val:
Last metadata expiration check: 5:32:11 ago on Tue 15 Nov 2022 10:00:33 AM CET.
Dependencies resolved.
============================================================================================================================================================
 Package                                          Architecture                 Version                                Repository                       Size
============================================================================================================================================================
Installing:
[...]
```
- vous pouvez déterminer dans quel paquet est disponible la commande `mysql` en saisissant `des mysqdnf provil`
- **donc vous devez effectuer une commande `mysql` sur `web.tp2.linux`**
- une fois connecté à la base, utilisez les commandes SQL fournies ci-dessous pour explorer la base

```sql
SHOW DATABASES;
USE <DATABASE_NAME>;
SHOW TABLES;
```

```bash
[val@web ~]$ sudo mysql -u nextcloud -h 10.102.1.12 -p
[sudo] password for val:
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
[...]
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> USE nextcloud;
Database changed
mysql> SHOW TABLES;
Empty set (0.00 sec)bash
```


🌞 **Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données**

> Les utilisateurs de la base de données sont différents des utilisateurs du système Rocky Linux qui porte la base. Les utilisateurs de la base définissent des identifiants utilisés pour se connecter à la base afin d'y voir ou d'y modifier des données.

```bash
MariaDB [(none)]> SELECT User, Host FROM mysql.user;
+-------------+-------------+
| User        | Host        |
+-------------+-------------+
| nextcloud   | 10.102.1.11 |
| mariadb.sys | localhost   |
| mysql       | localhost   |
| root        | localhost   |
+-------------+-------------+
4 rows in set (0.012 sec)
```

Une fois qu'on s'est assurés qu'on peut se co au service de base de données depuis `web.tp2.linux`, on peut continuer.

### B. Serveur Web et NextCloud

⚠️⚠️⚠️ **N'OUBLIEZ PAS de réinitialiser votre conf Apache avant de continuer. En particulier, remettez le port et le user par défaut.**

🌞 **Install de PHP**

```bash
# On ajoute le dépôt CRB
[val@web ~]$  sudo dnf config-manager --set-enabled crb
[sudo] password for val:
```

```bash
# On ajoute le dépôt REMI
[val@web ~]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
Rocky Linux 9 - BaseOS                                                                  7.9 kB/s | 3.6 kB     00:00
Rocky Linux 9 - AppStream                                                               8.0 kB/s | 3.6 kB     00:00
Rocky Linux 9 - CRB                                                                     1.4 MB/s | 1.9 MB     00:01
remi-release-9.rpm                                                                      242 kB/s |  25 kB     00:00
Dependencies resolved.
[...]
```

```bash
# On liste les versions de PHP dispos, au passage on va pouvoir accepter les clés du dépôt REMI
[val@web ~]$ sudo dnf module list php
Extra Packages for Enterprise Linux 9 - x86_64                                          3.7 MB/s |  11 MB     00:03
Remi's Modular repository for Enterprise Linux 9 - x86_64                               2.8 kB/s | 833  B     00:00
Remi's Modular repository for Enterprise Linux 9 - x86_64                               3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x478F8947:
[...]
```

```bash
# On active le dépôt REMI pour récupérer une version spécifique de PHP, celle recommandée par la doc de NextCloud
[val@web ~]$ sudo dnf module enable php:remi-8.1 -y
Last metadata expiration check: 0:01:42 ago on Wed 16 Nov 2022 06:56:16 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                     Architecture               Version                       Repository                   Size
========================================================================================================================
Enabling module streams:
 php                                                    remi-8.1

Transaction Summary
========================================================================================================================

Complete!
```

```bash
# Eeeet enfin, on installe la bonne version de PHP : 8.1
[val@web ~]$ sudo dnf install -y php81-php
Last metadata expiration check: 0:02:11 ago on Wed 16 Nov 2022 06:56:16 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                                  Architecture       Version                        Repository             Size
========================================================================================================================
Installing:
[...]
```


🌞 **Install de tous les modules PHP nécessaires pour NextCloud**

```bash
# eeeeet euuuh boom. Là non plus j'ai pas pondu ça, c'est la doc :
[val@web ~]$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
Last metadata expiration check: 0:03:53 ago on Wed 16 Nov 2022 06:56:16 PM CET.
Package libxml2-2.9.13-1.el9_0.1.x86_64 is already installed.
Package openssl-1:3.0.1-41.el9_0.x86_64 is already installed.
Package php81-php-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
[...]
```

🌞 **Récupérer NextCloud**

- créez le dossier `/var/www/tp2_nextcloud/`

```bash
[val@web ~]$ ls -l /var/www/ | grep -i tp2_nextcloud
drwxr-xr-x. 2 root root 6 Nov 16 19:02 tp2_nextcloud
```

- récupérer le fichier suivant avec une commande `curl` ou `wget` : https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip

```bash
[val@web ~]$ wget https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
--2022-11-16 19:04:25--  https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
Resolving download.nextcloud.com (download.nextcloud.com)... 95.217.64.181, 2a01:4f9:2a:3119::181
Connecting to download.nextcloud.com (download.nextcloud.com)|95.217.64.181|:443... connected.
HTTP request sent, awaiting response... 200 OK
[...]
```

- extrayez tout son contenu dans le dossier `/var/www/tp2_nextcloud/` en utilisant la commande `unzip`
  - contrôlez que le fichier `/var/www/tp2_nextcloud/index.html` existe pour vérifier que tout est en place

```bash
[val@web ~]$ ls -l /var/www/tp2_nextcloud/ | grep -i index.html
-rw-r--r--.  1 val val   156 Oct  6 14:42 index.html
```

- assurez-vous que le dossier `/var/www/tp2_nextcloud/` et tout son contenu appartient à l'utilisateur qui exécute le service Apache

```bash
[val@web ~]$ sudo chown apache /var/www//tp2_nextcloud/ -R
[val@web ~]$ ls -al /var/www/tp2_nextcloud/
total 132
drwxr-xr-x. 14 apache root  4096 Nov 16 19:16 .
drwxr-xr-x.  5 root   root    54 Nov 16 19:02 ..
drwxr-xr-x. 47 apache val   4096 Oct  6 14:47 3rdparty
drwxr-xr-x. 50 apache val   4096 Oct  6 14:44 apps
-rw-r--r--.  1 apache val  19327 Oct  6 14:42 AUTHORS
drwxr-xr-x.  2 apache val     67 Oct  6 14:47 config
-rw-r--r--.  1 apache val   4095 Oct  6 14:42 console.php
-rw-r--r--.  1 apache val  34520 Oct  6 14:42 COPYING
drwxr-xr-x. 23 apache val   4096 Oct  6 14:47 core
-rw-r--r--.  1 apache val   6317 Oct  6 14:42 cron.php
drwxr-xr-x.  2 apache val   8192 Oct  6 14:42 dist
-rw-r--r--.  1 apache val    156 Oct  6 14:42 index.html
-rw-r--r--.  1 apache val   3456 Oct  6 14:42 index.php
drwxr-xr-x.  6 apache val    125 Oct  6 14:42 lib
-rw-r--r--.  1 apache val    283 Oct  6 14:42 occ
drwxr-xr-x.  2 apache val     23 Oct  6 14:42 ocm-provider
drwxr-xr-x.  2 apache val     55 Oct  6 14:42 ocs
drwxr-xr-x.  2 apache val     23 Oct  6 14:42 ocs-provider
-rw-r--r--.  1 apache val   3139 Oct  6 14:42 public.php
-rw-r--r--.  1 apache val   5426 Oct  6 14:42 remote.php
drwxr-xr-x.  4 apache val    133 Oct  6 14:42 resources
-rw-r--r--.  1 apache val     26 Oct  6 14:42 robots.txt
-rw-r--r--.  1 apache val   2452 Oct  6 14:42 status.php
drwxr-xr-x.  3 apache val     35 Oct  6 14:42 themes
drwxr-xr-x.  2 apache val     43 Oct  6 14:44 updater
-rw-r--r--.  1 apache val    387 Oct  6 14:47 version.php
```

🌞 **Adapter la configuration d'Apache**

- regardez la dernière ligne du fichier de conf d'Apache pour constater qu'il existe une ligne qui inclut d'autres fichiers de conf
- créez en conséquence un fichier de configuration qui porte un nom clair et qui contient la configuration suivante :

```bash
[val@web /]$ sudo cat /etc/httpd/conf.d/clair.conf
<VirtualHost *:80>
  DocumentRoot /var/www/tp2_nextcloud/ # on indique le chemin de notre webroot
  ServerName  web.tp2.linux # on précise le nom que saisissent les clients pour accéder au service
  <Directory /var/www/tp2_nextcloud/> # on définit des règles d'accès sur notre webroot
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

🌞 **Redémarrer le service Apache** pour qu'il prenne en compte le nouveau fichier de conf

```bash
[val@web /]$ sudo systemctl restart httpd
[val@web /]$ systemctl status httpd.service
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
    Drop-In: /usr/lib/systemd/system/httpd.service.d
             └─php81-php-fpm.conf
     Active: active (running) since Wed 2022-11-16 19:41:16 CET; 16s ago
       Docs: man:httpd.service(8)
   Main PID: 4326 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 213 (limit: 48946)
     Memory: 34.7M
        CPU: 222ms
     CGroup: /system.slice/httpd.service
             ├─4326 /usr/sbin/httpd -DFOREGROUND
             ├─4327 /usr/sbin/httpd -DFOREGROUND
             ├─4328 /usr/sbin/httpd -DFOREGROUND
             ├─4329 /usr/sbin/httpd -DFOREGROUND
             └─4330 /usr/sbin/httpd -DFOREGROUND

Nov 16 19:41:16 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Nov 16 19:41:16 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Nov 16 19:41:16 web.tp2.linux httpd[4326]: Server configured, listening on: port 80
```

### C. Finaliser l'installation de NextCloud

➜ **Sur votre PC**

- modifiez votre fichier `hosts` (oui, celui de votre PC, de votre hôte)
  - pour pouvoir joindre l'IP de la VM en utilisant le nom `web.tp2.linux`

```bash
PS C:\Users\bravo> ssh val@web.tp2.linux
The authenticity of host 'web.tp2.linux (10.102.1.11)' can't be established.
ED25519 key fingerprint is SHA256:nmKLJXCQWhry97nJGwX/9osqdQZE8sRmsUrKaW4DbeU.
This host key is known by the following other names/addresses:
    C:\Users\bravo/.ssh/known_hosts:37: 10.101.1.11
    C:\Users\bravo/.ssh/known_hosts:38: 10.101.1.12
    C:\Users\bravo/.ssh/known_hosts:39: 10.102.1.11
    C:\Users\bravo/.ssh/known_hosts:40: 10.102.1.12
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'web.tp2.linux' (ED25519) to the list of known hosts.
Last login: Wed Nov 16 18:03:43 2022 from 10.102.1.1
```

- avec un navigateur, visitez NextCloud à l'URL `http://web.tp2.linux`
  - c'est possible grâce à la modification de votre fichier `hosts`

```bash
PS C:\Users\bravo> curl http://web.tp2.linux


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html>
                    <head>
                        <script> window.location.href="index.php"; </script>
                        <meta http-equiv="refresh" content="0; URL=index.php">
                    </head>
                    </html>

RawContent        : HTTP/1.1 200 OK
                    Keep-Alive: timeout=5, max=100
                    Connection: Keep-Alive
                    Accept-Ranges: bytes
                    Content-Length: 156
                    Content-Type: text/html; charset=UTF-8
                    Date: Wed, 16 Nov 2022 19:15:03 GMT
                    ETag: "...
Forms             : {}
Headers           : {[Keep-Alive, timeout=5, max=100], [Connection, Keep-Alive], [Accept-Ranges, bytes],
                    [Content-Length, 156]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 156
```


🌴 **C'est chez vous ici**, baladez vous un peu sur l'interface de NextCloud, faites le tour du propriétaire :)

🌞 **Exploration de la base de données**

- connectez vous en ligne de commande à la base de données après l'installation terminée
- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation

```bash
MariaDB [nextcloud]> SHOW TABLES;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts                 |
[.............................]
| oc_whats_new                |
+-----------------------------+
95 rows in set (0.001 sec)
```

**95 tables ont été crées**
